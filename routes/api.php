<?php

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\AuthController@user');
    });
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('orders', function () {
    return response(Order::all(), 200);
});

Route::get('orders/{order}', function ($orderId) {
    return response(Order::find($orderId), 200);
});


Route::post('orders', function (Request $request) {
    $order = new Order();
    $order->save();
    $resp = Order::create($request->all());
    return $resp;

});

Route::put('orders/{order}', function (Request $request, $orderId) {
    $order = Order::findOrFail($orderId);
    $order->update($request->all());
    return $order;
});

Route::delete('orders/{order}', function ($orderId) {
    Order::find($orderId)->delete();

    return 204;

});
